<?php

declare(strict_types = 1);

namespace Drupal\custom_admin_url\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Access controller for the user.
 */
class AccessController extends ControllerBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a UserCustomAccessHandler object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   */
  public function __construct(Request $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    // @phpstan-ignore-next-line
    return new self($container->get('request_stack')->getCurrentRequest());
  }

  /**
   * Deny access to admin and users pages if is wrong BO URL.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The Accoount interface.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Allow if is good back office URL.
   */
  public function access(AccountInterface $account): AccessResultInterface {
    $current_host = $this->request->getHost();
    $current_route_name = $this->request->attributes->get('_route');
    $config = $this->config('custom_admin_url.settings');
    $bo_url = $config->get('bo_url');
    if ($bo_url) {
      // Displays 403 page if it tries to login from front-office sub-domain.
      if ($current_host !== $bo_url) {
        return AccessResult::forbidden();
      }
    }
    // Allow user login.
    if ($current_route_name === 'user.login') {
      return AccessResult::allowed();
    }
    // Allow only if account as access administration pages permission.
    return AccessResult::allowedIfHasPermissions($account, ['access administration pages']);
  }

}
