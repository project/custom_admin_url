<?php

declare(strict_types=1);

namespace Drupal\custom_admin_url\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Custom admin URL settings for this site.
 */
final class CustomAdminUrlForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'custom_admin_url_custom_admin_url';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['custom_admin_url.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['bo_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Back Office URL'),
      '#description' => $this->t('Set Back-office URL. Eg: back.exemple.com'),
      '#default_value' => $this->config('custom_admin_url.settings')->get('bo_url'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $url = $form_state->getValue('bo_url');
    if (is_string($url)) {
      if (!UrlHelper::isValid($url, FALSE)) {
        $form_state->setErrorByName('bo_url', $this->t('The url is not valid. An absolute url has to be provided.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $bo_url = $form_state->getValue('bo_url');
    $this->config('custom_admin_url.settings')
      ->set('bo_url', $bo_url)->save();
    parent::submitForm($form, $form_state);
  }

}
