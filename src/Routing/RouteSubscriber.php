<?php

declare(strict_types = 1);

namespace Drupal\custom_admin_url\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * List of admin paths.
   *
   * @var array
   */
  public const ADMIN_PATHS = ['admin', 'user'];

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection): void {
    /** @var \Symfony\Component\Routing\Route $route */
    foreach ($collection as $route) {
      $path = \explode('/', \rtrim($route->getPath(), '/'));
      if (isset($path[1]) && \in_array($path[1], self::ADMIN_PATHS, TRUE)) {
        $route->addRequirements([
          '_custom_access' => '\Drupal\custom_admin_url\Controller\AccessController::access',
        ]);
      }
    }
  }

}
