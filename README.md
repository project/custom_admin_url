# Custom admin URL

Little module for restrict Back office path by URL.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

After enabling the module, go on a config page
`admin/config/system/custom-admin-url` and set back-office URL

## Maintainers

Current maintainers:
* Ines WALLON - [liber_t](https://www.drupal.org/u/liber_t)

Supporting organizations:
* [AlmaviaCX](https://drupal.org/almavia-cx)

